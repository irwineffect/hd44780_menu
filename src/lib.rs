#![no_std]

// Allow std library for tests
#[cfg(test)]
#[macro_use]
extern crate std;

use core::convert::TryInto;

const STACK_SIZE: usize = 4;

#[derive(Debug, Clone, Copy)]
pub enum Event {
    Up,
    Down,
    Left,
    Right,
    Enter,
    Back,
}

pub trait CharacterDisplay: core::fmt::Write  {
    fn set_line(&mut self, line: u8);
    fn clear(&mut self);
    fn finish_line(&mut self, len: usize) {
        let remaining = 20 - len - 1;
        for _ in 0..remaining {
            self.write_str(" ").unwrap();
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Context<'a, A>
where A: core::marker::Copy
{
    submenu: &'a Submenu<'a, A>,
    current_selection: usize,
    visible_start: usize,
    visible_end: usize,
}

#[derive(Debug)]
pub struct Menu<'a, A>
where A: core::marker::Copy
{
    menus: &'a Submenu<'a, A>,
    context: [Context<'a, A>; STACK_SIZE],
    index: usize,
}

#[derive(Debug)]
pub struct SubmenuOption<'a, A> {
    pub text: &'a str,
    pub sub: &'a Option<'a, A>,
}

#[derive(Debug)]
pub enum Option<'a, A> {
    Submenu(Submenu<'a, A>),
    Action(A),
}

#[derive(Debug)]
pub struct Submenu<'a, A> {
    pub title: &'a[&'a str],
    pub options: &'a[&'a SubmenuOption<'a, A>],
}

impl<'a, A> Menu<'a, A>
where A: core::marker::Copy
{
    pub fn new(menus: &'a Submenu<'a, A>) -> Self {
        let mut m = Menu {
            menus: menus,
            index: 0,
            context: [Context {
                submenu: menus,
                current_selection: 0,
                visible_start: 0,
                visible_end: 0,
            }; STACK_SIZE],
        };

        m.initialize_context(m.menus);

        m
    }

    fn initialize_context<'b:'a>(& mut self, submenu: &'b Submenu<A>) {
        let c = &mut self.context[self.index];
        assert!(submenu.title.len() < 4);
        let remaining_lines = 4 - submenu.title.len();
        let num_options = submenu.options.len();

        let _visible_options = if remaining_lines > num_options {
            num_options
        }
        else {
            remaining_lines
        };

        c.submenu = submenu;
        c.current_selection = 0;
        c.visible_start = 0;
    }

    pub fn draw<T: CharacterDisplay>(&self, display: &mut T) {
        let c = self.context[self.index];
        let m = c.submenu;
        //display.clear();

        let mut j = 0;
        for i in (0..4).zip(m.title.iter()) {
            let (line_num, line_text) = i;
            j += 1;
            display.set_line(line_num.try_into().unwrap());
            write!(display, "{}", line_text).unwrap();
            display.finish_line(line_text.len());
        }

        for i in (j..4).zip(m.options.iter().enumerate().skip(c.visible_start)) {
            let (line_num, (option_num, option)) = i;
            j += 1;

            display.set_line(line_num);
            let cursor = if (option_num as usize) == c.current_selection {
                '>'
            }
            else {
                ' '
            };

            let line_text = option.text;
            write!(display, "{}{}", cursor, line_text).unwrap();
            display.finish_line(line_text.len()+1);
        }

        for i in j..4 {
            display.set_line(i);
            display.finish_line(0);
        }
    }

    pub fn event(&mut self, e: Event) -> core::option::Option<&A> {
        let mut c = &mut self.context[self.index];
        let s = c.submenu;
        match e {
            Event::Up => {
                if c.current_selection > 0 {
                    c.current_selection -= 1;
                    if c.current_selection < c.visible_start {
                        c.visible_start -= 1;
                    }
                }
            },
            Event::Down => {
                if (c.current_selection + 1) < s.options.len() {
                    c.current_selection += 1;
                    let visible_end = c.visible_start + 4 - s.title.len() - 1;
                    if c.current_selection > visible_end {
                        c.visible_start += 1;
                    }
                }
            },
            Event::Enter => {
                if s.options.len() > 0 {
                    let s = s.options[c.current_selection].sub;
                    match s {
                       Option::Submenu(menu) => {
                            self.index += 1;
                            self.initialize_context(menu);
                       },
                       Option::Action(a) => {
                           return core::option::Option::Some(a);
                       }
                    }
                }
            },
            Event::Back => {
                if self.index > 0 {
                    self.index -= 1;
                }
            },
            _ => {}
        }

        core::option::Option::None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    struct ConsoleDisplay;

    impl CharacterDisplay for ConsoleDisplay {
        fn set_line(&mut self, line: u8) {
            if line == 0 {
                print!("\n\n\n\n***********************");
            }
            print!("\nline{}: ", line);
        }

        fn clear(&mut self) {
            println!("\n\n\n\n***********************");
        }
    }
    impl core::fmt::Write for ConsoleDisplay {
        fn write_str(&mut self, s: &str) -> Result<(), core::fmt::Error> {
            print!("{}", s);
            Ok(())
        }
    }

    #[derive(Debug, Copy, Clone)]
    enum Actions {
        ActionA,
        ActionB,
    }

    #[test]
    fn test_new() {
        let mut d = ConsoleDisplay;
        let mut m = Menu::<Actions>::new(
            &Submenu {
                title: &["This is the title",
                  "title line 2",],
                options: &[
                    &SubmenuOption {
                        text: "option a",
                        sub: &Option::Submenu(Submenu {
                            title: &["Inside option A"], options: &[]
                        }),
                    },
                    &SubmenuOption {
                        text: "option b",
                        sub: &Option::Action(Actions::ActionB,),
                    },
                    &SubmenuOption {
                        text: "option C",
                        sub: &Option::Submenu(Submenu {
                            title: &["Inside option C"], options: &[]
                        }),
                    },
                ]
            }
        );

        let events = vec!(
            Event::Enter,
            Event::Down,
            Event::Back,
            Event::Down,
            Event::Enter,
            Event::Enter,
            Event::Back,

            );

        for e in events {
            m.draw(&mut d);
            let a = m.event(e);
            if let Some(a) = a {
               println!("\nAction! {:?}", a);
            }
        }
        m.draw(&mut d);

        println!("\n");
    }
}
